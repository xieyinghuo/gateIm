<?php
namespace gateim;

trait Config{
    /*
    * redis 配置
    */
    protected $redis_config = [
        'host' => '127.0.0.1',                  // redis 主机地址
        'prefix' => 'gateim:',                   // key 前缀，避免运行多个时redis键冲突
        'password' => 'asbngpk*12',             // redis 密码
        'port' => 6379,                         // redis 端口
        'select' => 0,                          // redis db 库, 建议显示指定 1-15 的数字均可
        'timeout' => 0,                         // redis 超时时间
        'persistent' => false,                  // redis 持续性，连接复用
    ];

    /*
    * websocket server 监听端口
    */
    protected static $websocket = [
        'ServiceHost' => 'swoole.gzyxkk.com',     // webSocket服务地址，可放ip,域名,域名需解析到本服务器上
        'ServicePort' =>  9502,                  // webSocket服务端口
    ];


    /*
     * server 性能配置
     * 更多配置查看 swoole 官方文档
     * https://www.swoole.com/
    */
    protected static $server_config = [
        'worker_num' => 4,                       // 设置启动的 Worker 进程数
        'task_worker_num' => 0,                  // 设置异步任务的工作进程数量,最大值不得超过 CPU核心数 * 1000, 开启越多越占用内存
        'daemonize' => false,                    // 是否守护进程运行，生产模式必开
        'tcp_keepidle' => 30,                    // 30s没有数据传输就进行检测
        'tcp_keepinterval' => 1,                 // 1s探测一次
        'tcp_keepcount' => 1,                    // 探测的次数，超过5次后还没回包close此连接

        // wss配置文件配置，wss 应用中，发起 WebSocket 连接的页面必须使用 HTTPS
        'openSll' => true, // 是否开启ssl
        'ssl_cert_file' => __DIR__.'/ssl_file/cert.pem',
        'ssl_key_file' => __DIR__.'/ssl_file/key.pem',
    ];


}