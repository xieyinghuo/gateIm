<?php
namespace gateim\library\action;

/*
 * 发送消息类
*/
trait GetData
{
    // 获取在线的 fd 数量
    public function getFdsNum(){
        return $this -> redis -> zCard($this -> redis_key['bind_fd_uid']);
    }

    // 获取在线的 uid 数量
    public function getUidsNum(){
        $fds = $this -> redis -> zRange($this -> redis_key['bind_fd_uid'],0,-1,true);
        return count(array_unique(array_values($fds)));
    }

    // 获取 group_id 中 uid 的数量
    public function getGroupIdInUidNum($group_id){
        return $this -> redis -> sCard($this -> redis_key['groupList'].$group_id);
    }

    // 获取 uid 加入 group_id 的数量
    public function getUidToGroupIDNum($uid){
        return $this -> redis -> sCard($this -> redis_key['uidList'].$uid);
    }

    // 获取 uid 中 fd 的数量
    public function getUidBindFdNum($uid){
        return $this -> redis -> zCount($this -> redis_key['bind_fd_uid'],$uid,$uid);
    }

    // 判断 uid 是否在线
    public function isUidOnline($uid){
        $num = $this ->getUidBindFdNum($uid);
        return $num > 0;
    }

    // 判断 group_id 是否存在
    public function isGroupIdOnline($group_id){
        $num = $this -> getGroupIdInUidNum($group_id);
        return $num > 0;
    }




}