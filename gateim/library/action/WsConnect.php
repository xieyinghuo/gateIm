<?php
namespace gateim\library\action;

use gateim\Config;
use gateim\library\server\Redis;

/*
 * 处理服务中 fd客户端码 、 uid用户id 、 group_id群组id 之间的关系
*/
class WsConnect
{
    use Config;
    use SendMsg; // 引入发送类
    use GetData; // 引入获取数据类
    private $redis = null; // redis 实例
    private $redis_key = [
        "bind_fd_uid" => "bind_fd_uid",
        "groupList" => "groupList:group_id:",
        "uidList" => "uidList:uid:"
    ];
    private $server = null; // 服务实例

    // 构造方法
    public function __construct($server=null,$redis=null){
        $this -> server = $server;

        // 当有连接池的redis传进来，使用连接池中的，没有则实例化 redis
        if(empty($redis)){
            $this -> redis = (new Redis())->getRedis();
        }else{
            $this -> redis = $redis;
        }

        // 给表名添加前缀
        foreach ($this -> redis_key as $key => $value){
            $this -> redis_key[$key] = $this->redis_config['prefix'].$this -> redis_key[$key];
        }
    }

    // 清空 redis 中的数据
    public function emptyRedis(){
        foreach ($this -> redis_key as $key => $value) {
            $this->redis->del($this->redis_key[$key]);
        }
    }

    // 将fd与uid绑定
    public function bind_fd_uid($fd,$uid){
        $this -> redis -> zadd($this -> redis_key['bind_fd_uid'], $uid , $fd);
    }

    // 获取所有fd
    public function getFdAll(){
        $data = $this -> redis -> zrange($this -> redis_key['bind_fd_uid'], 0, -1);
        return $data;
    }

    // 获取uid所有的fd
    public function getUserSonFd($uid){
        $data = $this -> redis -> zrangebyscore($this -> redis_key['bind_fd_uid'], $uid, $uid);
        return $data;
    }

    // 获取某个fd所属的uid
    public function getFdOnUid($fd){
        $data = $this -> redis -> zscore($this -> redis_key['bind_fd_uid'], $fd);
        return $data;
    }

    // 移除uid以及它的所有fd
    public function delUidAndFd($uid){
        $fd_list = $this -> getUserSonFd($uid);
        foreach ($fd_list as $fd){
            $this -> server->close($fd);
        }
        $this -> redis -> zremrangebyscore($this -> redis_key['bind_fd_uid'], $uid, $uid);
        $this -> uidRemoveAllGroupId($uid);
    }

    // 移除某个fd
    public function delFd($fd){
        // 获取 uid ,查看是否还有其他 fd，没有清空 group_id
        $this -> server->close($fd);
        $this -> redis -> zrem($this -> redis_key['bind_fd_uid'], $fd);
    }

    // 将 uid 加入一个 group_id 中
    public function uidToGroupID($uid,$group_id){
        $this -> redis ->sadd($this -> redis_key['groupList'].$group_id, $uid);
        $this -> redis ->sadd($this -> redis_key['uidList'].$uid, $group_id);
    }

    // 将 uid 移出 group_id
    public function uidRemoveGroupId($uid,$group_id){
        $this -> redis -> srem($this -> redis_key['groupList'].$group_id, $uid);
        $this -> redis -> srem($this -> redis_key['uidList'].$uid, $group_id);
    }

    // 获取 uid 加入的所有 group_id
    public function getUidToGroupId($uid){
        $data = $this -> redis -> smembers($this -> redis_key['uidList'].$uid);
        return $data;
    }

    // 获取 group_id 中的所有 uid
    public function getGroupIdInUid($group_id){
        $data = $this -> redis -> smembers($this -> redis_key['groupList'].$group_id);
        return $data;
    }

    // 将 uid 移除所有 group_id
    public function uidRemoveAllGroupId($uid){
        // 查询 uid 在的所有 group_id ,并将 uid 移出 group_id
        $GroupList = $this -> getUidToGroupId($uid);
        foreach($GroupList as $val){
            $this -> redis -> srem($this -> redis_key['groupList'].$val, $uid);
        }

        $this -> redis -> del($this -> redis_key['uidList'].$uid);
    }

     // 解散 group_id
    public function removeGroupId($group_id){
        $this -> redis -> del($this -> redis_key['groupList'].$group_id);
    }





}