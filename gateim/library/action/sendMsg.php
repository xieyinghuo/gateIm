<?php
namespace gateim\library\action;

/*
 * 发送消息类
*/
trait SendMsg
{
    // 给所有 fd 发送消息
    public function sendFds($msg){
        $fd_list = $this -> getFdAll();
        $i = 0;
        foreach ($fd_list as $value){
            $i ++;
            $this -> server ->push($value, $msg);
        }
        return $i;
    }

    // 给指定 fd 发送消息
    public function sendFd($fd,$msg){
        $this -> server ->push($fd, $msg);
    }

    // 给指定 uid 发送消息
    public function sendUid($uid,$msg){
        $fd_list = $this -> getUserSonFd($uid);
        $i = 0;
        foreach ($fd_list as $value){
            $i ++;
            $this -> server ->push($value, $msg);
        }
        return $i;
    }

    // 给指定 group_id 发送消息
    public function sendGroupId($group_id,$msg){
        $uid_list = $this -> getGroupIdInUid($group_id);
        $i = 0;
        foreach ($uid_list as $value){
            $i ++;
            $this -> sendUid($value,$msg);
        }
        return $i;
    }




}