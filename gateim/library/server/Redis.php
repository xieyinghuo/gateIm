<?php
namespace gateim\library\server;

use gateim\Config;

/*
 * redis 连接类
*/
class Redis
{
    use Config;
    protected $handler = null;

    /**
     * 构造函数
     */
    public function __construct()
    {
        if (!extension_loaded('redis')) {
            echo "缺少 redis 扩展";die();
        }

        // 获取 redis 配置
        $this->handler = new \Redis();
        if ($this->redis_config['persistent']) {
            $this->handler->pconnect($this->redis_config['host'], $this->redis_config['port'], $this->redis_config['timeout'], 'persistent_id_' . $this->redis_config['select']);
        } else {
            $this->handler->connect($this->redis_config['host'], $this->redis_config['port'], $this->redis_config['timeout']);
        }

        if (!empty($this->redis_config['password'])) {
            $this->handler->auth($this->redis_config['password']);
        }

        $this->handler->select($this->redis_config['select']);
    }

    public function getRedis() {
        return $this->handler;
    }
}